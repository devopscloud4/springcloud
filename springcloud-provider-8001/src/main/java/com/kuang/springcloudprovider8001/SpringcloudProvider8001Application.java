package com.kuang.springcloudprovider8001;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
public class SpringcloudProvider8001Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProvider8001Application.class, args);
    }

    //增加一个servlet 配置一个bean  让dashboard能够监控到  (导入Hystrix依赖)--固定格式代码
    @Bean
    public ServletRegistrationBean HystrixMetricsStreamServlet (){
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new HystrixMetricsStreamServlet());
        //监控的路径
        registrationBean.addUrlMappings("/actuator/hystrix.stream");
        return registrationBean;
    }

}
