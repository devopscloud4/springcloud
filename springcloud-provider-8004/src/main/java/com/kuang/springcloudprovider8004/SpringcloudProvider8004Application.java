package com.kuang.springcloudprovider8004;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SpringcloudProvider8004Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProvider8004Application.class, args);
    }

}
