package com.kuang.springcloudprovider8003.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProvideController {
    /**
     * 暴露两个接口
     * @param a
     * @param b
     * @return
     */
    /**
     * 服务提供者与数据库交互，给消费者提供数据
     * @param a
     * @param b
     * @return
     */
    @GetMapping("/jiafa")
    public Integer jiafa(@RequestParam int a, @RequestParam int b){
        return a+b;
    }

    @GetMapping("/subtraction")
    public Integer subtraction(@RequestParam int a,@RequestParam int b){
        return a-b;
    }

    @GetMapping("/kakaf{age}")
    public String subtraction(@PathVariable int age){
        return "我今年"+age+"岁";
    }
}
