package com.kuang.springcloudproviderhystrix8005.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author yuan jie
 * @Date 2021/4/18 23:18
 */
@Slf4j
@RestController
public class ProvideController {
    /**
     * 暴露两个接口
     * @param a
     * @param b
     * @return
     */
    /**
     * 服务提供者与数据库交互，给消费者提供数据
     * @param a
     * @param b
     * @return
     */
    //服务熔断(服务提供端)的测试接口，服务降级是在消费端进行
    @GetMapping("/jiafa")
    @HystrixCommand(fallbackMethod = "getMessage")
    public String jiafa(@RequestParam int a, @RequestParam int b){
        log.info("进入熔断接口....");
        if (a == 9){
            throw new RuntimeException("错误！");
        }
        return "结果为："+(a+b);
    }
//    jiafa方法雪崩后Hystrix调用的返回消息方法(备份方法)服务熔断
    //因为指定的 备用方法 和 原方法 的参数个数，类型不同，就会报错
    public String getMessage(@RequestParam int a, @RequestParam int b){

        return "a不能为9，请稍后再试(服务熔断)....";
    }

    @GetMapping("/subtraction")
    public Integer subtraction(@RequestParam int a,@RequestParam int b){
        return a-b;
    }

    @GetMapping("/kakaf{age}")
    public String subtraction(@PathVariable int age){
        return "我今年"+age+"岁";
    }
}
