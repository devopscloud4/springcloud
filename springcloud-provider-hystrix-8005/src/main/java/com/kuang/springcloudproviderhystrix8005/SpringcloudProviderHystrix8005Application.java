package com.kuang.springcloudproviderhystrix8005;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableDiscoveryClient
//@EnableCircuitBreaker 已废弃
@EnableHystrix
public class SpringcloudProviderHystrix8005Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProviderHystrix8005Application.class, args);
    }

}
