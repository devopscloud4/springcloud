package com.kuang.springcloudproviderhystrix8005.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Author yuan jie
 * @Date 2021/4/20 14:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Test {
    private String date;
    private String name;
}