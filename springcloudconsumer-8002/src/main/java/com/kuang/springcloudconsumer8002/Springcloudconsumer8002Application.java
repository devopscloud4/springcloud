package com.kuang.springcloudconsumer8002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient //让注册中心能够发现，扫描到该服务
@EnableFeignClients  //启用feign客户端功能，并扫描所有的@FeignClient远程接口
public class Springcloudconsumer8002Application {

    public static void main(String[] args) {
        SpringApplication.run(Springcloudconsumer8002Application.class, args);
    }

}
