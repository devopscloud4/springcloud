package com.kuang.springcloudconsumer8002.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 调用外部服务的接口 Feign 方式
 * @Author yuan jie
 * @Date 2021/4/18 21:38
 */
//fallbackFactory = FeignFallbackFactory.class  批量熔断降级
@FeignClient(value = "provide-hystrix",fallbackFactory = FeignFallbackFactory.class) //服务提供者的服务名
@Component
public interface Feign {

    /**
     * 以下全是服务提供者的 controller层 暴露的接口 把他作为接口到此消费
     * @param a
     * @param b
     * @return
     */
    //服务熔断接口(服务提供端进行)，指服务端发生错误进行熔断(备用方法返回提示信息)
    @GetMapping("/jiafa")
    String jiafa(@RequestParam int a, @RequestParam int b);

    @GetMapping("/subtraction")
    Integer subtraction(@RequestParam int a,@RequestParam int b);
    //服务降级接口(消费端进行),将此服务暂时关闭，把资源给流量大的服务，流量过后，在开启此服务
    //测试的时候将服务关闭，客户端会提示：客户端提供了降级的信息，这个服务已关闭！
    @GetMapping("/kakaf{age}")
    String kakaf(@PathVariable int age);
}
