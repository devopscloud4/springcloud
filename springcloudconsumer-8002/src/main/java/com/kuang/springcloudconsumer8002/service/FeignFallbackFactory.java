package com.kuang.springcloudconsumer8002.service;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Author yuan jie
 * @Date 2021/4/20 22:09
 */
//服务降级
@Component
public class FeignFallbackFactory implements FallbackFactory {
    @Override
    public Object create(Throwable cause) {
        return new Feign() {
            @Override
            public String jiafa(int a, int b) {
                return null;
            }

            @Override
            public Integer subtraction(int a, int b) {
                return null;
            }

            @Override
            public String kakaf(int age) {
                return "客户端提供了降级的信息(服务降级)，这个服务已关闭！"+age;
            }
        };
    }
}
