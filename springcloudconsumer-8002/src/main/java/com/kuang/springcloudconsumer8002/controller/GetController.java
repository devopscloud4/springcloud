package com.kuang.springcloudconsumer8002.controller;

import com.kuang.springcloudconsumer8002.service.Feign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author yuan jie
 * @Date 2021/4/18 21:38
 */
@RestController
public class GetController {
//    装配接口
    @Autowired
    private Feign feign;

    /**
     * 调用外部服务接口，获得数据
     */

    @GetMapping("/jiafa/{a}/{b}")
    public String jiafa(@PathVariable int a,@PathVariable int b){
        String result = feign.jiafa(a,b);
        System.out.println("调用服务提供者方法provide-hystrix中jiafa方法："+result);
        return "调用服务提供者方法provide-hystrix中jiafa方法："+result;
    }

    @GetMapping("/subtraction")
    public String subtraction(){
        Integer result = feign.subtraction(5, 20);
        System.out.println("调用服务提供者方法provide-hystrix中subtraction方法："+result);
        return "调用服务提供者方法provide-hystrix中subtraction方法："+result;
    }

    //请求：http://localhost:8002/kakaf/30/袁杰
    @GetMapping("/kakaf/{age}/{name}")
    public String kakaf(@PathVariable Integer age,@PathVariable String name){
        String s = feign.kakaf(age);
        System.out.println("调用服务提供者方法provide-hystrix中kakaf方法："+s+name);
        return "调用服务提供者方法provide-hystrix中kakaf方法："+s+name;
    }
}
