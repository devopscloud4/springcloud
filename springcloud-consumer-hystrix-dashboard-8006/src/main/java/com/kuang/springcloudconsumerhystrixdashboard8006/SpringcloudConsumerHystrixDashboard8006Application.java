package com.kuang.springcloudconsumerhystrixdashboard8006;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

@SpringBootApplication
//http://localhost:8006/hystrix
//开启监控  服务端需导入 监控信息的依赖 actuator
@EnableHystrixDashboard
public class SpringcloudConsumerHystrixDashboard8006Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudConsumerHystrixDashboard8006Application.class, args);
    }

}
